
import Prismic from 'prismic-javascript';


function PrismicData(type, language) {
    const apiEndPoint = `https://quren-reader.cdn.prismic.io/api/v2`;

    Prismic.api(apiEndPoint)
        .then((api) => {


            api.query(
                Prismic.Predicates.at('document.type', type),
                { lang: '*' }

            )
                .then((response) => {


                    if (response) {
                        const dataBack = {
                            english: response.results[0],
                            myanmar: response.results[1]
                        }
                        return dataBack;

                    }







                })





        })
}

export default PrismicData