
import React from 'react'
import { Route, Switch } from 'react-router-dom'


import Home from './components/landing/Home'
import Login from './components/login-signup/admin/Login/Login'

import Authenticated from './components/misc/auth/Authenticated'

import AdminRoutes from './components/private/AdminRoutes'
import ModalManager from './components/misc/modalManager/modalManager';






const Routes = () => {





  return (




    <div>

      <ModalManager />

      <Switch>
        <Route exact component={Home} path="/" />
        <Route exact component={Login} path="/login-admin" />
        <AdminRoutes />

      </Switch>

    </div>




  )
}




export default Routes;

