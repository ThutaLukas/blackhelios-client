import React, { Component, Fragment } from 'react'
import Navbar from '../../navigation/NavBar'
import Footer from '../Footer/Footer'
import Authenticated from '../../misc/auth/Authenticated'



class HeaderFooter extends Component {




    render() {



        return (
            <Fragment>
                <Navbar user={this.props.user} />
                {this.props.children}
                <Footer />
            </Fragment>
        )
    }
}





export default Authenticated(HeaderFooter, true);